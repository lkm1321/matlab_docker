# Matlab docker file. 
# Forked from Ben Heasly's Dockerfile
# 
FROM ubuntu:16.04

MAINTAINER Brian Lee <brian.lee@student.uts.edu.au> 

# Matlab dependencies
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    libpng12-dev libfreetype6-dev \
    libblas-dev liblapack-dev gfortran build-essential xorg libx11-dev libxt6 libxt-dev\
    && rm -rf /var/lib/apt/lists/* 

RUN mkdir -p /rt_root && mkdir -p /matlab_runtime && \
    wget http://www.mathworks.com/supportfiles/downloads/R2018b/deployment_files/R2018b/installers/glnxa64/MCR_R2018b_glnxa64_installer.zip && \
    mkdir -p /mcr-install && cd mcr-install && \
    unzip -q MCR_R2018b_glnxa64_installer.zip && \
    ./install -destinationFolder /matlab_runtime -agreeToLicense yes -mode silent && \
    cd / && rm -rf mcr-install 

# These are the directories expected by the config. 
RUN mkdir -p /apps && mkdir -p /logs && mkdir -p /config
# VOLUME ["/apps", "/logs", "/config"]
EXPOSE 8080

ENV MCR_ROOT /matlab_runtime
ENV LD_LIBRARY_PATH "$LD_LIBRARY_PATH:$MCR_ROOT/runtime/glnxa64:$MCR_ROOT/bin/glnxa64:$MCR_ROOT/sys/os/glnxa64:$MCR_ROOT/sys/opengl/lib/glnxa64"
ENV XAPPLRESDIR /matlab_runtime/X11/app-defaults

CMD ["/matlab_runtime/bin/glnxa64/webapps", "--configFile", "/config/webapps.config"]
WORKDIR /rt_root

