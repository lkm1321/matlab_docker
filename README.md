## Installation

1. Install docker-ce
2. Get usage script by:  
`docker run lkm1321/matlab:runtime > matlab_docker`
3. Install the usage script (optional): 
`chmod +x matlab_docker && sudo cp matlab_docker /usr/bin/matlab_docker`

## Commands

`matlab_docker build [arguments] <m-file-you-want>`

Allowed arguments are:
--name=[name of image]: name of image (defaults to name of m file). 
--mcc=[mcc location]: location of mcc compiler (if invalid, matlab-docker will try to compile from within matlab -nojvm -nodisplay). 

`matlab_docker run [arguments] <image-you-want> <commands> 

Allowed arguments are: 
--mount-x11: whether to mount x11 (). 
--mount-dir=<dirname>: directory to mount (default none). 
--daemonise: run in background (default false). 
--docker-options: additional options to pass to docker.


