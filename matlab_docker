#!/bin/bash

set -e

ROOT=rt_root
IMAGE_NAME=lkm1321/matlab:runtime

function usage
{
    echo "usage: matlab_docker [arguments] [command]" 
    echo " "
    echo "Allowed arguments: " 
    echo "-b, --background: run in background" 
    echo "-c, --cli: command-line only. no GUI will be produced (i.e. no X11 forwarding)"
    echo "-d, --source-dir: source directory. defaults to pwd"
    echo "--docker-options: additional options to pass to docker"
    echo "--docker: custom docker command (e.g. nvidia-docker)"
    echo "-m [folder-name], --mount-dir [folder-name]: mount folder [folder-name]. /folder1/folder2/folder3 will be mounted at /\$ROOT/folder3"
    echo "--nvidia: use options for nvidia"
    echo "--no-rm: don't remove the container on exit (removes by default)"
    echo "--name: container name (default: matlab_docker)"
    echo "-v, --verbose: verbose mode" 
    echo "-h, --help: display this help message and exit"
}

function verbose
{
    if [ ! -z $options_verbose ]; then
        echo "$1" >&2
    fi
}


function parse_args()
{
    # positional arguments (the command) 
    args=()
    # Directories to mount. 
    options_mount_dir=()

    # named arguments
    while [ "$1" != "" ]; do
        case "$1" in
            -b | --background ) options_background="1";;
            -c | --cli ) options_cli_only="1";;
            -d | --source-dir ) options_source_dir="$2"; shift;;
            --docker-options ) options_docker_append="$2"; shift;;
            --docker ) options_docker="$2"; shift;;
            -m | --mount-dir ) options_mount_dir+=("$2"); shift;;
            -h | --help ) usage; exit;;
            --nvidia ) options_nvidia="1";;
            --no-rm ) options_no_rm="1";;
            --name ) options_name="$2"; shift;;
            -v | --verbose ) options_verbose="1";;
            *) args+=("$1")
        esac
        shift
    done
    
    if [ -z $options_source_dir ]; then
        echo "Warning: assuming --source-dir is current directory ($(pwd))"
        options_source_dir="$(pwd)"
    fi

    if [ -z $options_docker ]; then
        options_docker="docker"
    fi

    if [ -z $options_name ]; then
        options_name="matlab_docker"
    fi

    echo $args
}


function get_docker_options()
{
    local DOCKER_OPTIONS=""
    
    # Run in background? background is -d (daemonise). Otherwise, -it. 
    if [ ! -z $options_background ]; then 
        verbose "Docker will be daemonised (run in background)" 
        DOCKER_OPTIONS="-d $DOCKER_OPTIONS"
    else
        verbose "Docker will be run in terminal mode"
        DOCKER_OPTIONS="-it $DOCKER_OPTIONS"
    fi

    ## Remove the container by default. 
    if [ -z $options_no_rm ]; then
        verbose "Container will be removed on exit"
        DOCKER_OPTIONS="$DOCKER_OPTIONS --rm"
    fi

    ## Container name. 
    if [ ! -z $options_name ]; then
        verbose "Container name is $options_name"
        DOCKER_OPTIONS="$DOCKER_OPTIONS --name=$options_name" 
    fi

    ## Additional options to docker. 
    if [ ! -z "$options_docker_append" ]; then
        verbose "Appending $options_docker_append to docker options"
        DOCKER_OPTIONS="$DOCKER_OPTIONS $options_docker_append"
    fi

    # Mount all the directories. 
    for mount_dir in ${options_mount_dir[@]}; do
        verbose "Mounting $mount_dir at /$ROOT/$(basename $mount_dir)"
        DOCKER_OPTIONS="$DOCKER_OPTIONS -v $mount_dir:/$ROOT/$(basename $mount_dir)"
    done

    # Mount source dir
    if [ ! -z $options_source_dir ]; then
        verbose "source directory is $options_source_dir" >&2
        DOCKER_OPTIONS="$DOCKER_OPTIONS -v $options_source_dir:/$ROOT"
    fi

    # Options required for gui
    if [ -z $options_cli_only ]; then
        verbose "Mounting X11 stuff for forwarding... " >&2
        DOCKER_OPTIONS="$DOCKER_OPTIONS -v /tmp/.X11-unix:/tmp/.X11-unix:ro"
        DOCKER_OPTIONS="$DOCKER_OPTIONS -e DISPLAY=:0"
        DOCKER_OPTIONS="$DOCKER_OPTIONS -e QT_X11_NO_MITSHM=1"
        DOCKER_OPTIONS="$DOCKER_OPTIONS -e JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8"
        DOCKER_OPTIONS="$DOCKER_OPTIONS -e XAUTHORITY=/.Xauthority"
    fi

    # Nvidia env variables (use for nvidia-docker). 
    if [ ! -z $options_nvidia]; then
        verbose "Defining environment variables for nvidia-docker" >&2
        DOCKER_OPTIONS="$DOCKER_OPTIONS -e NVIDIA_VISIBLE_DEVICES=all"
        DOCKER_OPTIONS="$DOCKER_OPTIONS -e SVGA_VGPU10=0" 
    fi

    # Call with command substitution!
    echo "$DOCKER_OPTIONS"
}

function run()
{
    if [ -z $options_cli_only ]; then
        verbose "Enabling X access control from docker to host... "
        xhost + > /dev/null
    fi
    
    if [ ! -z $options_verbose ]; then
        verbose "Running: run $(get_docker_options) $IMAGE_NAME ${args[@]}"
    fi 

    $options_docker run $(get_docker_options) $IMAGE_NAME ${args[@]}
    
    if [ -z $options_cli_only ]; then
        if [ ! -z $options_verbose ]; then
            verbose "Disabling X access control from docker to host... "
        fi
        xhost - > /dev/null
    fi
}

function check_docker_exists()
{
    if [ -z $(command -v $options_docker) ]; then
        echo "You don't seem to have $options_docker installed!" 
        echo "docker-ce can be installed by following: https://docs.docker.com/install/linux/docker-ce/ubuntu/#set-up-the-repository" 
        exit 1
    fi
}


# Main section

# Remember to dobule quote here, otherwise double-quoted spaces get ignored. 
parse_args "$@"
check_docker_exists
run
